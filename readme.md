# Пример запросов через curl

###1. Клиент делает запрос на создание документа
```sh
curl --location --request POST 'http://127.0.0.1:8000/api/v1/document' \
--header 'Accept: application/json'
```

###2. Клиент редактирует документ первый раз
```sh
curl --location --request POST 'http://127.0.0.1:8000/api/v1/document/8bc5aeda-95b8-4085-a313-3205c1dbe533' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "document": {
        "payload": {
            "actor": "The fox",
            "meta": {
                "type": "quick",
                "color": "brown"
            },
            "actions": [
                {
                    "action": "jump over",
                    "actor": "lazy dog"
                }
            ]
        }
    },
    "_method": "PUT"
}'
```

###3. Клиент делает запрос на создание документа
```sh
curl --location --request POST 'http://127.0.0.1:8000/api/v1/document/8bc5aeda-95b8-4085-a313-3205c1dbe533' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "document": {
        "payload": {
            "meta": {
                "type": "cunning",
                "color": null
            },
            "actions": [
                {
                    "action": "eat",
                    "actor": "blob"
                },
                {
                    "action": "run away"
                }
            ]
        }
    },
    "_method": "PUT"
}'
```

###4. Клиент публикует документ
```sh
curl --location --request POST 'http://127.0.0.1:8000/api/v1/document/8bc5aeda-95b8-4085-a313-3205c1dbe533/publish' \
--header 'Accept: application/json'
```

###5. Клиент получает запись в списке
```sh
curl --location --request GET 'http://127.0.0.1:8000/api/v1/document?page=1'
```
так же доступен ключ в запросе `perPage=20`
