<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Requests\DocumentRequest;
use App\Http\Resources\DocumentCollection;
use App\Http\Resources\DocumentResource;
use Illuminate\Support\Arr;

class DocumentController extends Controller
{
    /**
     * Возвращает документы
     * @return DocumentCollection
     */
    public function index()
    {
        return new DocumentCollection(
            Document::orderBy('created_at', 'DESC')->paginate(request('perPage', 20))
        );
    }

    /**
     * Создает новый документ
     * @param DocumentRequest $request
     * @return DocumentResource
     */
    public function store(DocumentRequest $request)
    {
        $document = Document::create($request->json('document.payload', ['payload' => []]));

        return new DocumentResource(
            $document->refresh()
        );
    }

    /**
     * Возвращает документ по ID
     * @param Document $document
     * @return DocumentResource
     */
    public function show(Document $document)
    {
        return new DocumentResource($document);
    }

    /**
     * Обновляет документ
     * @param DocumentRequest $request
     * @param Document $document
     * @return DocumentResource
     * @throws \Exception
     */
    public function update(DocumentRequest $request, Document $document)
    {
        if(!$request->json('document.payload')) {
            throw new \Exception('payload не передан!', 400);
        }

        if($document->isPublished()) {
            throw new \Exception('Документ уже опубликован!', 400);
        }

        $document->update(Arr::get($request->validated(), 'document'));

        return new DocumentResource($document);
    }

    /**
     * Публикует документ
     * @param Document $document
     * @return DocumentResource
     */
    public function publish(Document $document)
    {
        if(!$document->isPublished()) {
            $document->update(['status' => Document::PUBLISHED]);
        }

        return new DocumentResource($document);
    }
}
