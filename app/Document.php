<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Document extends Model
{
    const DRAFT = 'draft';
    const PUBLISHED = 'published';

    const ALL_STATUSES = [
        self::DRAFT,
        self::PUBLISHED,
    ];

    /**
     * Тип для поля ID меняем на string в связи с использованием uuid
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Отключаем инкерементацию поля ID, так как используется uuid
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'payload',
        'status'
    ];

    protected $casts = [
        'payload' => 'array'
    ];

    protected static function boot()
    {
        parent::boot();

        // генерируем UUID;
        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4());
        });
    }

    /**
     * Проверяет статус публикации документа
     * @return bool
     */
    public function isPublished()
    {
        return $this->attributes['status'] === self::PUBLISHED;
    }
}
